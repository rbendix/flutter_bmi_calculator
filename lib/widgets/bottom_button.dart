import 'package:flutter/material.dart';

import '../constants.dart';

class BottomButton extends StatelessWidget {

  final String label;
  final Function onPress;

  BottomButton({@required this.label, @required this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        child: Text(label, style: calculateTextStyle),
        alignment: Alignment.center,
        height: 80,
        margin: EdgeInsets.only(top: 10),
        width: double.infinity,
        color: Color(0xFFEB1555),
      ),
    );
  }
}