import 'package:flutter/material.dart';

class RoundIconButton extends StatelessWidget {
  final IconData icon;
  final Function onPress;

  RoundIconButton({this.icon, this.onPress});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPress,
      elevation: 0.0,
      constraints: BoxConstraints.tight(Size(56.0, 56.0)),
      child: Icon(icon, color: Colors.white,),
      shape: CircleBorder(),
      fillColor: Color(0xFF4C4F5E),
    );
  }
}
