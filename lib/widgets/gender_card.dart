import 'package:bmi_calculator/constants.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class GenderCard extends StatelessWidget {

  Gender gender;

  GenderCard({this.gender});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          gender == Gender.female ? FontAwesomeIcons.venus : FontAwesomeIcons.mars,
          size: 80,
        ),
        SizedBox(height: 20),
        Text(
          gender == Gender.female ? 'FEMALE' : 'MALE',
          style: cardTextStyle,
        )
      ],
    );
  }
}

enum Gender {
  female,
  male
}