import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PanelCard extends StatelessWidget {
  Color color;
  Widget child;
  Function onPress;

  PanelCard({@required this.color, this.child, this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        margin: EdgeInsets.all(15),
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.circular(10)),
        child: child,
      ),
    );
  }
}
