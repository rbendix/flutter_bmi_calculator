import 'package:flutter/material.dart';

const cardTextStyle = TextStyle(fontSize: 18, color: Color(0xFF8D8E98));
const calculateTextStyle = TextStyle(fontSize: 24, color: Colors.white, fontWeight: FontWeight.w900);
const numberTextStyle = TextStyle(fontSize: 50.0, fontWeight: FontWeight.w900);
const resultTextStyle = TextStyle(color: Colors.green, fontSize: 22, fontWeight: FontWeight.bold);
const interpretationTextStyle = TextStyle(fontSize: 22);

const Color activeColor = Color(0xFF1D1E33);
const Color inactiveColor = Color(0xFF111328);
