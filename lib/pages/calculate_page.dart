import 'package:bmi_calculator/widgets/bottom_button.dart';
import 'package:bmi_calculator/widgets/panel_card.dart';
import 'package:flutter/material.dart';

import '../constants.dart';

class CalculateArguments {
  final String bmiResult;
  final String resultText;
  final String resultInterpretation;

  CalculateArguments({this.bmiResult, this.resultText, this.resultInterpretation});
}

class CalculatePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final CalculateArguments args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text("BMI Calculator"),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(15),
                child: Text("Your Result", style: TextStyle(fontSize: 65)),
              ),
            ),
            Expanded(
              flex: 5,
              child: PanelCard(
                color: activeColor,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        args.resultText.toUpperCase(),
                        style: resultTextStyle,
                      ),
                      Text(
                        args.bmiResult,
                        style: numberTextStyle.copyWith(
                            fontSize: 100, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        args.resultInterpretation,
                        textAlign: TextAlign.center,
                        style: interpretationTextStyle,
                      )
                    ]),
              ),
            ),
            BottomButton(
              label: 'RE-CALCULATE',
              onPress: () => Navigator.pushNamed(context, '/'),
            ),
          ]),
    );
  }
}
